# Installation guide

1) Clone project
2) Run command -> *composer install*
3) Copy and paste **.env.example** (Rename it to **.env**)
4) Change DB configs in **.env** file as you wish
5) Run command -> *php artisan generate:descriptor*
6) Enter required parameters
7) See the results

**Enjoy**
