<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Command\Command as CommandAlias;

class SegmentDescriptor extends Command
{
    /**
     * Delimiters & prefix constants
     */
    const PREFIX = 'PT';
    const HOURS_DELIMITER = 'H';
    const MINUTES_DELIMITER = 'M';
    const SECONDS_DELIMITER = 'S';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:descriptor';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates the segment descriptor';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Checks if file exists and readable
     *
     * @param string $path
     * @return bool
     */
    public function isReadableXmlFileExists(string $path): bool
    {
        return (file_exists($path) && is_file($path) && is_readable($path) && pathinfo($path)['extension'] === 'xml');
    }

    /**
     * Converts string time to number of seconds
     *
     * @param string $string
     * @return float
     */
    public function convertStringToSeconds(string $string): float
    {
        $string = trim(str_replace([self::PREFIX], '', $string));
        $descriptorPos = 0;
        $seconds = 0.0;

        $hoursDelimiterPos = strpos($string, self::HOURS_DELIMITER);
        if ($hoursDelimiterPos !== false) {
            $seconds += (floatval(substr($string, $descriptorPos, $hoursDelimiterPos)) * 60 * 60);
            $descriptorPos = $hoursDelimiterPos + 1;
        }

        $minutesDelimiterPos = strpos($string, self::MINUTES_DELIMITER);
        if ($minutesDelimiterPos !== false) {
            $seconds += (floatval(substr($string, $descriptorPos, $minutesDelimiterPos)) * 60);
            $descriptorPos = $minutesDelimiterPos + 1;
        }

        $secondsDelimiterPos = strpos($string, self::SECONDS_DELIMITER);
        if ($secondsDelimiterPos !== false) {
            $seconds += floatval(substr($string, $descriptorPos, $secondsDelimiterPos));
        }

        return $seconds;
    }

    /**
     * Converts number of seconds to string time
     *
     * @param float $seconds
     * @return string
     */
    public function convertSecondsToString(float $seconds): string
    {
        $string = self::PREFIX;

        $hours = floor($seconds / 60 / 60);
        if ($hours > 0) {
            $seconds -= $hours * 60 * 60;
            $string .= $hours . self::HOURS_DELIMITER;
        }

        $minutes = floor($seconds / 60);
        if ($minutes > 0) {
            $seconds -= $minutes * 60;
            $string .= $minutes . self::MINUTES_DELIMITER;
        }

        $string .= round($seconds, 3) . self::SECONDS_DELIMITER;

        return $string;
    }

    /**
     * Processes data into JSON format
     *
     * @param string $file
     * @param float $silence_duration
     * @param float $segment_duration
     * @param float $split_silence_duration
     * @return false|null|string
     */
    public function processData(string $file, float $silence_duration,
                                float $segment_duration, float $split_silence_duration): ?string
    {
        $data = simplexml_load_file($file);

        if ($data === false || $split_silence_duration >= $silence_duration) {
            return false;
        }

        $result = [];
        $points = [];
        $chapterNumber = 1;
        $prevStartPoint = 0;
        $chapterLength = 0;

        foreach ($data as $object) {
            if(!isset($object['from']) || !isset($object['until'])) continue;

            $secondsFrom = $this->convertStringToSeconds($object['from']);
            $secondsUntil = $this->convertStringToSeconds($object['until']);

            $chapterLength += $secondsFrom - $prevStartPoint;
            if ($secondsUntil - $secondsFrom >= $split_silence_duration) {
                array_push($points, $prevStartPoint);
                $prevStartPoint = $secondsUntil;
            }

            if ($secondsUntil - $secondsFrom >= $silence_duration) {
                if ($chapterLength >= $segment_duration) {
                    foreach ($points as $index => $point) {
                        array_push($result, [
                            'title' => 'Chapter ' . $chapterNumber .
                                (count($points) > 1 ? ', part ' . ($index + 1) : ''),
                            'offset' => $this->convertSecondsToString($point)
                        ]);
                    }
                }
                else {
                    array_push($result, [
                        'title' => 'Chapter ' . $chapterNumber,
                        'offset' => $this->convertSecondsToString($points[0])
                    ]);
                }

                $points = [];
                $chapterLength = 0;
                $chapterNumber++;
            }
        }

        return (count($result) > 0 ? json_encode(['segments' => $result],
            JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE |JSON_NUMERIC_CHECK | JSON_PRETTY_PRINT)
            : null);
    }

    /**
     * Saves string data to a text file
     *
     * @param string $data
     * @return string|false
     */
    public function saveDataToFile(string $data): ?string
    {
        $filename = 'silence_descriptor_' . date('dmy_His') . '.txt';
        $file = fopen($filename, 'x');

        if ($file !== false) {
            fputs($file, $data);
            $message = $filename;
        }
        else {
            $message = false;
        }

        fclose($file);

        return $message;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        while (!isset($filepath) || $this->isReadableXmlFileExists($filepath) === false) {
            if (isset($filepath)) {
                $this->error('File does not exists, or is not readable, or has non XML extension!');
            }
            $filepath = $this->ask('Set the path to the XML-file with data');
        }
        $this->line('Filepath is set to ' . $filepath);

        while (!isset($silence_duration) || floatval($silence_duration) <= 0) {
            if (isset($silence_duration)) {
                $this->error('Value is incorrect!');
            }
            $silence_duration = $this->ask(
                'Set the silence duration in seconds, which reliably indicates a chapter transition
                (must be higher than zero)'
            );
        }
        $this->line('Silence duration is set to ' . floatval($silence_duration) . 's');

        while (!isset($segment_duration) || floatval($segment_duration) <= 0) {
            if (isset($segment_duration)) {
                $this->error('Value is incorrect!');
            }
            $segment_duration = $this->ask(
                'Set the maximum duration of a segment in seconds, after which the chapter will be broken up
                into multiple segments'
            );
        }
        $this->line('Segment duration is set to ' . floatval($segment_duration) . 's');

        while (!isset($split_silence_duration) || floatval($split_silence_duration) <= 0
            || floatval($split_silence_duration) >= $silence_duration) {
            if (isset($split_silence_duration)) {
                $this->error('Value is incorrect!');
            }
            $split_silence_duration = $this->ask(
                'Set a silence duration in seconds, which can be used to split a long chapter
                (must be shorter than the silence duration used to split chapters)'
            );
        }
        $this->line('Split silence duration is set to ' . floatval($split_silence_duration) . 's');

        $result = $this->processData($filepath, $silence_duration, $segment_duration, $split_silence_duration);
        if ($result === false) {
            $this->error('Something went wrong while processing data!');
        }
        else if($result === null) {
                $this->warn('Result is empty! Check your XML-file for the correctness of the data');
        }
        else {
            $choices = [
                1 => 'In console',
                2 => 'In file'
            ];

            $output = $this->choice(
                'Do you want to get data output to a file or to the console?',
                $choices
            );

            switch(array_search($output, $choices)) {
                case 2: {
                    $savingResult = $this->saveDataToFile($result);

                    if($savingResult !== false) {
                        $this->info('Data was successfully saved to a file "' . $savingResult . '"');
                    }
                    else {
                        $this->error('Something went wrong while saving data in file!');
                    }

                    break;
                }
                default: {
                    print_r($result);
                    break;
                }
            }
        }

        return CommandAlias::SUCCESS;
    }
}
